package com.example

import models.Wines
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.execution.streaming.CommitMetadata.format
import org.json4s.jackson.JsonMethods.parse

object JsonReader extends App {

  val spark = SparkSession
    .builder()
    .getOrCreate()

  import spark.implicits._

  val filename = if (args.length == 0) {
    "winemag-data-130k-v2.json"
  } else {
    args(0)
  }

  val rdd = spark.sparkContext.textFile(filename).map(row =>
    parse(row).extract[Wines])
  rdd.toDF().show()
}
