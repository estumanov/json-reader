package models

case class Wines(
                val id: String,
                val country: String,
                val points: Int,
                val title: String,
                val variety: String,
                val winery: String
                )
